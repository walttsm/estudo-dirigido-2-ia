# Estudo dirigido #2 - Inteligência Artificial

## Executando os arquivos

Para executar os scripts basta abrir uma instância de terminal em sua máquina e executar o arquivo com o comando:

```bash
python3 caminho/do/arquivo.py
```

## Exercício 1 - Mudança

Este programa calcula se é possível realizar uma mudança com os caminhões contratados.

O método de cálculo recebe como parâmetro uma lista com os volumes dos objetos a serem transportados e outra com o volume máximo de transporte de cada caminhão.

A resposta devolvida é se é possível realizar a mudança.

## Exercício 2 - Sudoku

Este programa consiste num resolvedor de problemas de sudoku usando backtracking.

O problema a ser resolvido é modelado como uma matriz 9x9 fornecida no início do script.

O programa retorna como resposta o estado inicial do tabuleiro e o tabuleiro resolvido.
