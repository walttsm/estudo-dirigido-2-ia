def eh_possivel_transportar(objetos:[int], caminhoes:[int]) -> bool:
    volume_transportado = sum(objetos)
    max_volume = sum(caminhoes)

    if max_volume >= volume_transportado:
        caminhao_maior = max(caminhoes)
        for objeto in objetos:
            if objeto > caminhao_maior:
                return False

        return True
    else:
        return False


if __name__ == '__main__':
    objetos = [4,5,2,2,2]
    caminhoes = [3,8,5]
    res = eh_possivel_transportar(objetos, caminhoes)
    if res:
        print('É possível transportar a mudança!')
    else:
        print('Não é possível transportar toda a mudança')
