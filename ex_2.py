tab_inicial = [
    [5,3,0,0,7,0,0,0,0],
    [6,0,0,1,9,5,0,0,0],
    [0,9,8,0,0,0,0,6,0],
    [8,0,0,0,6,0,0,0,3],
    [4,0,0,8,0,3,0,0,1],
    [7,0,0,0,2,0,0,0,6],
    [0,6,0,0,0,0,2,8,0],
    [0,0,0,4,1,9,0,0,5],
    [0,0,0,0,8,0,0,7,9]
]

def print_tab(tab:[[int]]):
    for row in range(len(tab)):
        if row % 3 == 0 and row != 0:
            print('------------')

        for col in range(len(tab[0])):
            if col % 3 == 0 and col != 0:
                print('|', end='')
            
            if col == 8:
                print(tab[row][col])
            else:
                print(tab[row][col], end='')

def encontrar_vazio(tab):
    for row in range(len(tab)):
        for col in range(len(tab[0])):
            if tab[row][col] == 0:
                return (row, col)
    
    return None

def nao_ha_violacao(tab, num, pos):
    #pos[0] -> linha da posição
    #pos[1] -> coluna da posição

    # Checando por números iguais na linha da posição
    for i in range(len(tab[0])):
        if tab[pos[0]][i] == num and pos[1] != i:
            return False

    # Checando por números iguais na coluna da posição
    for i in range(len(tab)):
        if tab[i][pos[1]] == num and pos[0] != i:
            return False

    # Checando caixa 3x3 
    cx_x = pos[1] // 3
    cx_y = pos[0] // 3

    for i in range(cx_y*3, cx_y*3 + 3):
        for j in range(cx_x * 3, cx_x*3 + 3):
            if tab[i][j] == num and (i,j) != pos:
                return False

    return True

def resolve(tab):
    vazio = encontrar_vazio(tab)
    if not vazio:
        return True
    else:
        row, col = vazio # Desestrutura a lista

    for i in range(1,10):
        if nao_ha_violacao(tab, i, (row, col)):
            #print(row, col)
            tab[row][col] = i

            if resolve(tab):
                return True

            # Backtrack
            tab[row][col] = 0

    return False

print_tab(tab_inicial)
resolve(tab_inicial)
print("\nSolucionado:")
print_tab(tab_inicial)